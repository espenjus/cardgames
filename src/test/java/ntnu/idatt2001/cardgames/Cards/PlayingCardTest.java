package ntnu.idatt2001.cardgames.Cards;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;

public class PlayingCardTest {

    private char testSuit, falseSuit;
    private int testFace, falseFace, falseFace2;

    @BeforeEach
    void setup() {
        testSuit = 'H';
        falseSuit = 'A';
        testFace = 13;
        falseFace = 14;
        falseFace2 = 0;
    }

    @Test
    @DisplayName("Test the PlayingCard constructor")
    public void constructorTest() throws FileNotFoundException {
        PlayingCard card = new PlayingCard(testSuit, testFace);
        assertEquals(testSuit, card.getSuit());
        assertEquals(testFace, card.getFace());
    }

    @Test
    @DisplayName("Test the exceptions in Playingcard constructor")
    public void exceptionConstructorTest() {
        assertThrows(IllegalArgumentException.class, () -> { PlayingCard card = new PlayingCard(falseSuit, testFace);});
        assertThrows(IllegalArgumentException.class, () -> { PlayingCard card = new PlayingCard(testSuit, falseFace);});
        assertThrows(IllegalArgumentException.class, () -> { PlayingCard card = new PlayingCard(testSuit, falseFace2);});
    }

}
