package ntnu.idatt2001.cardgames.Cards;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;

public class DeckOfCardsTest {

    @Test
    @DisplayName("Test the constructor for DeckOfCards")
    public void DeckOfCardsConstructorTest() throws FileNotFoundException {
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52, deck.getCards().size());
        //check that all cards is in the deck
        for (int i = 0; i < deck.getCards().size(); i++) {
            assertNotNull(deck.getCards().get(i));
        }
    }

    @Test
    @DisplayName("Test the dealHand method")
    public void dealHandTest() throws FileNotFoundException {
        int n = 5;
        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = deck.dealHand(5);
        assertFalse(hand.getCards().isEmpty());
        assertEquals(5, hand.getCards().size());
        for (int i = 0; i < hand.getCards().size(); i++) {
            assertNotNull(hand.getCards().get(i));
        }
    }

    @Test
    @DisplayName("Test the exceptions in dealHand method")
    public void exceptionsInDealHandMethodTest() throws FileNotFoundException {
        int impossibleHand1 = 53;
        int impossibleHand2 = 0;
        DeckOfCards deck = new DeckOfCards();
        assertThrows(IllegalArgumentException.class, () -> { HandOfCards hand = deck.dealHand(impossibleHand1);});
        assertThrows(IllegalArgumentException.class, () -> { HandOfCards hand = deck.dealHand(impossibleHand2);});

    }

}
