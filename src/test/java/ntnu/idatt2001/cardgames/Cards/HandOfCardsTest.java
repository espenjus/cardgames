package ntnu.idatt2001.cardgames.Cards;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class HandOfCardsTest {

    private PlayingCard card1, card2, card3, card4, card5, queenOfSpades;
    private ArrayList<PlayingCard> cards, cardsWithQueenOfSpades;
    private HandOfCards testAndFlushHand, testQueenOfSpadesHand;

    @BeforeEach
    public void setup() throws FileNotFoundException {
        card1 = new PlayingCard('H', 1);
        card2 = new PlayingCard('H', 2);
        card3 = new PlayingCard('H', 3);
        card4 = new PlayingCard('H', 4);
        card5 = new PlayingCard('H', 5);
        queenOfSpades = new PlayingCard('S', 12);

        cards = new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);
        cards.add(card4);
        cards.add(card5);
        //fills hand with all hearts
        testAndFlushHand = new HandOfCards(cards);

        cardsWithQueenOfSpades = new ArrayList<>();
        cardsWithQueenOfSpades.add(queenOfSpades);
        cardsWithQueenOfSpades.add(card1);
        cardsWithQueenOfSpades.add(card2);
        cardsWithQueenOfSpades.add(card3);
        cardsWithQueenOfSpades.add(card4);
        // fills hand with four hearts and the queen of spades
        testQueenOfSpadesHand = new HandOfCards(cardsWithQueenOfSpades);
    }

    @Test
    @DisplayName("Test the HandOfCards constructor")
    public void handOfCardsConstructorTest() {
        HandOfCards hand = new HandOfCards(cards);
        assertEquals(cards, hand.getCards());
    }

    @Test
    @DisplayName("Test the getSumOfCards method")
    public void getSumOfCardsMethodTest() {
        assertEquals(15, testAndFlushHand.getSumOfCards());
    }

    @Test
    @DisplayName("Test the getAllHearts method")
    public void getAllHeartsMethodTest() {
        assertEquals(5, testAndFlushHand.getAllHearts().size());
        ArrayList<PlayingCard> test = new ArrayList<>();
        HandOfCards testHand = new HandOfCards(test);
        assertEquals(0, testHand.getAllHearts().size());
    }

    @Test
    @DisplayName("Test the hasQueenOfSpades method")
    public void hasQueenOfSpadesMethodTest() {
        assertTrue(testQueenOfSpadesHand.hasQueenOfSpades());
        assertFalse(testAndFlushHand.hasQueenOfSpades());
    }

    @Test
    @DisplayName("Test the hasFlush method")
    public void hasFlushMethodTest() {
        assertTrue(testAndFlushHand.hasFlush());
        assertFalse(testQueenOfSpadesHand.hasFlush());
    }
}
