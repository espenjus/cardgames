package ntnu.idatt2001.cardgames.Cards;


import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class representing a deck of playingcards. A deck contains 52 cards.13 of each sort H = hearts,
 * S = spades, D = diamonds, C = clubs
 */
public class DeckOfCards {

    private ArrayList<PlayingCard> cards;
    private final char[] suit = { 'S', 'H', 'D','C' };

    /**
     * constructor that adds all 52 playing cards to deck when initiated
     */
    public DeckOfCards() throws FileNotFoundException {

        cards = new ArrayList<>();

        for (int j = 0; j < suit.length; j++ ) {
            for (int i = 1; i < 14; i++) {
                cards.add(new PlayingCard(suit[j], i));
            }
        }
    }

    /**
     * get method for cards in deck
     * @return ArrayList of PlayingCards
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Method for dealing a random hand of cards
     * @param n number of cards in hand, can minimum be 1 and maximum be 52
     * @return hand of  cards
     */
    public HandOfCards dealHand(int n) {
        if (n < 1) throw new IllegalArgumentException("Minimum amount of cards per hand is 1");
        if (n > 52) throw new IllegalArgumentException("Maximum amunt of c ards per hand is 52");
        ArrayList<PlayingCard> copyListOfCards = new ArrayList<>(getCards());
        Random rand = new Random();
        ArrayList<PlayingCard> cardsInHand = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int nextCardIndex = rand.nextInt(copyListOfCards.size());
            cardsInHand.add(copyListOfCards.get(nextCardIndex));
            copyListOfCards.remove(copyListOfCards.get(nextCardIndex));
        }
        HandOfCards returnHand = new HandOfCards(cardsInHand);
        return returnHand;
    }

}
