package ntnu.idatt2001.cardgames.Cards;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * class for a hand of cards
 */

public class HandOfCards {

    private ArrayList<PlayingCard> cards;
    protected final static int CARDS_IN_HAND = 5;

    /**
     * constructor for a hand of cards
      * @param hand Arraylist of playingcards
     */
    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.cards = hand;
    }

    /**
     * get method for the cards in the hand
     * @return ArrayList of Playingcards
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * get method for the sum of the faces of cards in a hand
     * @return int sum of cards
     */
    public int getSumOfCards() {
        return cards.stream()
                .map(PlayingCard::getFace)
                .reduce((a, b) -> a+b).get();
    }

    /**
     * method for getting all the Heart cards in a hand
     * @return ArrayList of Hearts playingcards
     */
    public ArrayList<PlayingCard> getAllHearts() {
        return cards.stream()
                .filter(p -> p.getSuit() == 'H').
                collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * method for checking if the hand contains the Hearth of spades
     * @return boolean value
     */
    public boolean hasQueenOfSpades() {
        return cards.stream()
                .anyMatch(c -> c.getSuit() == 'S' && c.getFace() == 12);
    }

    /**
     * method for checking if the hand has a flush, five equal suits
     * @return boolean value
     */
    public boolean hasFlush() {
        if (cards.stream().filter(c -> c.getSuit() == 'H').count() >= 5) return true;
        if (cards.stream().filter(c -> c.getSuit() == 'S').count() >= 5) return true;
        if (cards.stream().filter(c -> c.getSuit() == 'D').count() >= 5) return true;
        if (cards.stream().filter(c -> c.getSuit() == 'C').count() >= 5) return true;
        return false;
    }

}
