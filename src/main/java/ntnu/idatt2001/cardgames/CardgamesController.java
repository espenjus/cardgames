package ntnu.idatt2001.cardgames;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ntnu.idatt2001.cardgames.Cards.DeckOfCards;
import ntnu.idatt2001.cardgames.Cards.HandOfCards;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;

public class CardgamesController implements Initializable {

    @FXML private Label sumOfFacesLabel, CardsOfHeartsLabel, QueenOfSpadesLabel, FlushLabel;
    @FXML private Button checkHandButton;
    @FXML private Button dealHandButton;
    @FXML private ImageView cardOne, cardTwo, cardThree, cardFour, cardFive;
    //private ImageView[] cardPlacements = {cardOne, cardTwo, cardThree, cardFour, cardFive};
    DeckOfCards deck;
    HandOfCards hand;





    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //sets the back of the cards to BackOfCard image.
        Image backOfCard = null;
        try {
            backOfCard = new Image(new FileInputStream("src/main/resources/ntnu/idatt2001/cardgames/Images/BackOfCard.jpg"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        cardOne.setImage(backOfCard);
        cardTwo.setImage(backOfCard);
        cardThree.setImage(backOfCard);
        cardFour.setImage(backOfCard);
        cardFive.setImage(backOfCard);

        sumOfFacesLabel.setText("N/A");
        CardsOfHeartsLabel.setText("N/A");
        QueenOfSpadesLabel.setText("N/A");
        FlushLabel.setText("N/A");
        }



    public void dealHand(ActionEvent actionEvent) throws FileNotFoundException {
        deck = new DeckOfCards();
        hand = deck.dealHand(5);
        cardOne.setImage(hand.getCards().get(0).getFrontImage());
        cardTwo.setImage(hand.getCards().get(1).getFrontImage());
        cardThree.setImage(hand.getCards().get(2).getFrontImage());
        cardFour.setImage(hand.getCards().get(3).getFrontImage());
        cardFive.setImage(hand.getCards().get(4).getFrontImage());
    }

    public void checkHand(ActionEvent actionEvent) {
        setSumOfFacesLabel();
        setCardsOfHeartsLabel();
        setQueenOfSpadesLabel();
        setFlushLabel();
    }

    public void setSumOfFacesLabel() {
        sumOfFacesLabel.setText(Integer.toString(hand.getSumOfCards()));
    }

    public void setCardsOfHeartsLabel() {
        if (hand.getAllHearts().size() >= 1) {
            String hearts = "";
            for (int i = 0; i < hand.getAllHearts().size(); i++) {
                hearts = hearts + " " + hand.getAllHearts().get(i).getAsString();
            }
            CardsOfHeartsLabel.setText(hearts);
        } else {
            CardsOfHeartsLabel.setText("No Hearts");
        }

    }

    public void setQueenOfSpadesLabel() {
        if (hand.hasQueenOfSpades()) {
            QueenOfSpadesLabel.setText("Yes");
        } else {
            QueenOfSpadesLabel.setText("No");
        }
    }

    public void setFlushLabel() {
        if (hand.hasFlush()) {
            FlushLabel.setText("Yes");
        } else {
            FlushLabel.setText("No");
        }
    }
}