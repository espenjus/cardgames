module ntnu.idatt2001.cardgames {
    requires javafx.controls;
    requires javafx.fxml;


    opens ntnu.idatt2001.cardgames to javafx.fxml;
    exports ntnu.idatt2001.cardgames;
    exports ntnu.idatt2001.cardgames.Cards;
    opens ntnu.idatt2001.cardgames.Cards to javafx.fxml;
}